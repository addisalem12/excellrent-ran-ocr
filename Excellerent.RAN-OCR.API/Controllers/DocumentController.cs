﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using System.Text.RegularExpressions;
using Google.Apis.Services;
using System.IO;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Win32.SafeHandles;
using Microsoft.Extensions.Configuration;
using Excellerent.RAN_OCR.API.Data;
using Excellerent.RAN_OCR.API.Service;
using Excellerent.RAN_OCR.API.Enum;
using Hangfire;

namespace Excellerent.RAN_OCR.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        private readonly DocumentService _service;
        private readonly EmailService _emailService;
        public DocumentController(RANOcrContext context, IWebHostEnvironment env) : base()
        {
            this._service = new DocumentService(context);
            this._emailService = new EmailService(context, env);
        }

        [HttpGet]
        public async Task<ActionResult<List<Document>>> GetAll(StatusEnum status)
        {
            var result = await _service.SearchAll(status);
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<ActionResult<Document>> Update(Document model)
        {
            var result = await this._service.Edit(model);
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}/UpdateStatus")]
        public async Task<ActionResult<Document>> UpdateStatus(StatusEnum status, int id)
        {
            var result = await this._service.UpdateStatus(id, status);
            return Ok(result);
        }

        [HttpGet]
        [Route("StartTask")]
        public ActionResult StartTask()
        {
            RecurringJob.AddOrUpdate("gmail-fetch", () => this._emailService.GetMessage(), "*/30 * * * * *");
            return Ok();
        }

    }
}