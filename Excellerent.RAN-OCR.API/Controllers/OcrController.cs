﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Excellerent.RAN_OCR.API.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Tesseract;

namespace Excellerent.RAN_OCR.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OcrController : ControllerBase
    {
        public const string folderName = "images/";
        public const string trainedDataFolderName = "tessdata";

        [HttpPost]
        public String DoOCR([FromForm] OcrModel request)
        {

            string name = request.Image.FileName;
            var image = request.Image;

            if (image.Length > 0)
            {
                using (var fileStream = new FileStream(folderName + image.FileName, FileMode.Create))
                {
                    image.CopyTo(fileStream);
                }
            }

            string tessPath = Path.Combine(trainedDataFolderName, "");
            string result = "";

            using (var engine = new TesseractEngine(tessPath, request.DestinationLanguage, EngineMode.Default))
            {
                using (var img = Pix.LoadFromFile(folderName + name))
                {
                    var page = engine.Process(img);
                    result = page.GetText();
                    Console.WriteLine(result);
                }
            }
            return String.IsNullOrWhiteSpace(result) ? "Ocr is finished. Return empty" : result;


        }

        [HttpPost]
        [Route("/extractInvoice")]
        public async Task<IActionResult> ExtractInvoiceAsync(string dir)
        {


            var client = new HttpClient();
            if (!string.IsNullOrEmpty(dir))
            {
                client.BaseAddress = new Uri("https://api.ocr.space/");
                client.DefaultRequestHeaders.Accept.Clear();

                var multiContent = new MultipartFormDataContent();
                var file = new StreamReader(dir);
                    if (file != null)
                {
                    var fileStreamContent = new StreamContent(file.BaseStream);
                    multiContent.Add(fileStreamContent, "fileToUpload", dir);
                   

                        multiContent.Add(new StringContent("true"), "isoverlayrequired");
                    multiContent.Add(new StringContent("true"), "istable");



                }


                multiContent.Headers.Add("apiKey", "c9cf1511a288957");

                var response = await client.PostAsync("parse/image", multiContent);
                if (response.IsSuccessStatusCode)
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    var ocrResult = OcrResult.FromJson(apiResponse);
                    var lines = ocrResult.ParsedResults[0].TextOverlay.Lines;
                    string fullData = ocrResult.ParsedResults[0].ParsedText;
                    string[] lines2 = fullData.Split('\n');
                    return Ok(apiResponse);
                }
            }

            return BadRequest();
        }

    }
}