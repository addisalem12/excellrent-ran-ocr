﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using System.Text.RegularExpressions;
using Google.Apis.Services;
using System.IO;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Win32.SafeHandles;
using Microsoft.Extensions.Configuration;

namespace Excellerent.RAN_OCR.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        public string[] Scopes = { GmailService.Scope.GmailReadonly, GmailService.Scope.GmailLabels, GmailService.Scope.GmailModify };  // change to Readonly 
        const string targetDirectoryPdf = @"C:\RanOcr\PDFS";
        public string ApplicationName = "eRtio OCR";
        private readonly IWebHostEnvironment _env;


        [HttpGet]
        public static List<Message> ListMessages(GmailService service, String userId, String query)
        {


            List<Message> result = new List<Message>();
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List(userId);
            request.Q = query;

            do
            {
                try
                {
                    ListMessagesResponse response = request.Execute();
                    result.AddRange(response.Messages);
                    request.PageToken = response.NextPageToken;
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                }
            } while (!String.IsNullOrEmpty(request.PageToken));

            return result;
        }
        public Message GetMessage()
        {
            UserCredential credential;
            string webRootPath = _env.WebRootPath;
            var pathToData = System.IO.Path.Combine(_env.ContentRootPath, "client_secret.json");
            using (var stream =
                new StreamReader(System.IO.File.OpenRead(pathToData)))
            {

                string credPath = "C:\\inetpub\\wwwroot\\Resource\\";
                credPath = Path.Combine(credPath, ".credentials/gmail-dotnet-quickstart.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream.BaseStream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);

            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            var mailList = ListMessages(service, "me", "is:unread has:attachment");

            if (mailList.Count > 0)
            {
                List<string> list = new List<string>();
                foreach (var message in mailList)
                {
                    list.Add(message.Id);
                    Message selectedMessage = service.Users.Messages.Get("me", message.Id).Execute();

                    string sender = "";
                    IList<MessagePart> parts = selectedMessage.Payload.Parts;
                    IList<MessagePartHeader> headers = selectedMessage.Payload.Headers;
                    foreach (var header in headers)
                    {
                        if (header.Name == "To" || header.Name == "to")
                        {
                            if (header.Value.Contains("<"))
                            {
                                var email = Regex.Matches(header.Value, @"<(.*?)>")[0];
                                sender = Regex.Replace(email.ToString(), @"<|>", "");
                            }
                            else
                                sender = header.Value;
                        }


                    }
                    int id = 0;
                    foreach (MessagePart part in parts)
                    {

                        if (!String.IsNullOrEmpty(part.Filename))
                        {
                            String attId = part.Body.AttachmentId;
                            MessagePartBody attachPart = service.Users.Messages.Attachments.Get("me", message.Id, attId).Execute();

                            // Converting from RFC 4648 base64 to base64url encoding
                            // see http://en.wikipedia.org/wiki/Base64#Implementations_and_history
                            String attachData = attachPart.Data.Replace('-', '+');
                            attachData = attachData.Replace('_', '/');

                            byte[] data = Convert.FromBase64String(attachData);

                            string pdfId = message.Id;

                            pdfId = message.Id + "-" + id;
                            string extension = Path.GetExtension(part.Filename);

                            System.IO.File.WriteAllBytes(Path.Combine(targetDirectoryPdf, pdfId + extension), data);
                            id = id + 1;

                        }
                    }
                    ModifyMessageRequest mods = new ModifyMessageRequest();
                    List<String> labelsToRemove = new List<string>(new[] { "UNREAD" });
                    mods.RemoveLabelIds = labelsToRemove;

                    try
                    {
                        service.Users.Messages.Modify(mods, "me", message.Id).Execute();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An error occurred: " + e.Message);

                    }
                }

            }
            return null;
        }
    }


}