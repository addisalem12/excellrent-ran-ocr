﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Data
{
    public class Invoice
    {
        public Invoice()
        {
            this.LineItems = new List<LineItem>();
        }

        public int Id { get; set; }
        public string InvoiceID { get; set; }
        public int DocumentId { get; set; }
        public string VendorName { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }

        public virtual Document Document { get; set; }
        public virtual ICollection<LineItem> LineItems { get; set; }
    }
}
