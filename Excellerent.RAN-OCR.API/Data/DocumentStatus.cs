﻿using Excellerent.RAN_OCR.API.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Data
{
    public class DocumentStatus
    {
        public int Id { get; set; }
        public int DocumentId { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public StatusEnum Status { get; set; }


        public virtual Document Document { get; set; }
    }
}
