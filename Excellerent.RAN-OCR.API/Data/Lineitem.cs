﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Data

{
    public class LineItem
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public string ItemName { get; set; }
        public Nullable<decimal> ItemQty { get; set; }
        public Nullable<decimal> ItemAmount { get; set; }


        public virtual Invoice Invoice { get; set; }
    }
}
