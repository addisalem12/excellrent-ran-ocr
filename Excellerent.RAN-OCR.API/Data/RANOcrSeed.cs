﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Data
{
    public static class RANOcrSeed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<RANOcrContext>();

            context.Database.EnsureCreated();

            if (!context.Invoices.Any())
            {
                context.Invoices.Add(new Invoice()
                {
                    InvoiceDate = DateTime.UtcNow,
                    TotalAmount = 250,
                    InvoiceID = "123-asd",
                    Document = new Document()
                    {
                        FileId = Guid.NewGuid().ToString(),
                        FileExt = ".pdf",
                        ReceivedDate = DateTime.UtcNow,
                        SenderEmail = "natan.tadesse@gmail.com",
                        DocumentStatuses = new List<DocumentStatus>()
                        {
                           new DocumentStatus()
                           {
                           Date = DateTime.UtcNow,
                           Status = Enum.StatusEnum.Received
                           }
                        }
                    },
                    LineItems = new List<LineItem>()
                    {
                       new LineItem()
                       {
                       ItemAmount = 100,
                       ItemName = "Item 1",
                       ItemQty = 1
                       },
                        new LineItem()
                       {
                       ItemAmount = 75,
                       ItemName = "Item 2",
                       ItemQty = 2
                       }
                    }
                });
                context.SaveChanges();
            }
        }
    }
}
