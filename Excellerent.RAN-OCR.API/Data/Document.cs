﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Data
{
    public class Document
    {
        public Document() 
        {
            this.DocumentStatuses = new List<DocumentStatus>();
        }
        public int Id { get; set; }
        public string SenderEmail { get; set; }
        public string FileId { get; set; }
        public string FileExt { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string JsonData { get; set; }


        public virtual List<DocumentStatus> DocumentStatuses { get; set; }
    }
}
