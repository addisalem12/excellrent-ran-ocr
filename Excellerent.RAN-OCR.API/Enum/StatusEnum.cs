﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Enum
{
    public enum StatusEnum
    {
        [Description("Unknown")]
        Unknown = 0,
        [Description("Received")]
        Received = 1,
        [Description("Succeeded")]
        Succeeded = 2,
        [Description("Failed")]
        Failed = 3,
        [Description("Approved")]
        Approved = 4,
    }
}
