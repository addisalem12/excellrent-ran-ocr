﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Model
{
    public class FormData
    {
      
        public IFormFile file { get; set; }
        public bool Isoverlayrequired { get; set; }
    }
}
