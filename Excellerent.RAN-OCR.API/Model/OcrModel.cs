﻿using Microsoft.AspNetCore.Http;
using System;

namespace Excellerent.RAN_OCR.API.Model
{
    public class OcrModel
    {
        public String DestinationLanguage { get; set; }
        public IFormFile Image { get; set; }
    }

}
