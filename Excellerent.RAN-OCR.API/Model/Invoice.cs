﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Model
{
    public class Invoice
    {
        public Invoice()
        {
            this.Lineitems = new List<LineItem>();
        }

        public string InvoiceID { get; set; }
        public string VendorName { get; set; }
        public string InvoiceDate { get; set; }
        public string TotalAmount { get; set; }
        public virtual ICollection<LineItem> Lineitems { get; set; }
    }
    public class Invoice2
    {
        
        public string InvoiceID { get; set; }
        public string VendorName { get; set; }
        public string InvoiceDate { get; set; }
        public string TotalAmount { get; set; }
       // public virtual ICollection<Lineitem> Lineitems { get; set; }
    }
}
