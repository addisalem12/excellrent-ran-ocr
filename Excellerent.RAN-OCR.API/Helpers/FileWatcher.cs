﻿using Excellerent.RAN_OCR.API.Controllers;
using Excellerent.RAN_OCR.API.Data;
using Excellerent.RAN_OCR.API.Service;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System.IO;

namespace Excellerent.RAN_OCR.API.Helpers
{
    public class FileWatcher
    {
        public static RANOcrContext _context;
        FileWatcher(RANOcrContext context) {
            _context = context;
        }
        public static void RegisterWatchers()
        {
            var dir = @"C:\ranOcr\pdfs";
             //"ConfigurationManager.AppSettings["PdfUrl"],
            var PDF = new FileSystemWatcher
            {
                Filter = "*.pdf",
                Path = dir, 
                EnableRaisingEvents = true,
                IncludeSubdirectories = false
            };

            PDF.Created += new FileSystemEventHandler(OnCreated);
        
        }

        static async void OnCreated(object sender, FileSystemEventArgs e)
        {
            string zipToUnpack = e.FullPath;
            // call extract image here 
            FileSystemWatcher send = (FileSystemWatcher)sender;

            //if (send.Path == ConfigurationManager.AppSettings["PdfUrl"])
            //{
            var connectionstring = "User ID=postgres;Password=postgres;Host=localhost;Port=5432;Database=RAN-OCR;Pooling=true;";

            var optionsBuilder = new DbContextOptionsBuilder<RANOcrContext>();
            optionsBuilder.UseNpgsql(connectionstring);


            RANOcrContext dbContext = new RANOcrContext(optionsBuilder.Options);
            var result = await new OCRService(dbContext).ExtractInvoiceAsync( e.FullPath);
            //}
           
        }
    }
}
