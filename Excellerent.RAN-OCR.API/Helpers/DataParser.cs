﻿using Excellerent.RAN_OCR.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Helpers
{
    public class DataParser
    {
        public List<Row> ConvertToRow(IList<Line> lines)
        {
            Identify strType = new Identify();

            List <Row> rows = lines.Select(x => new Row()
            {
                //do your variable mapping here 
                Line = 1,
                Colomun = 2,
                StringType =Convert.ToString(strType.StringType("")),
                Type =Convert.ToInt16( strType.StringType(
                                                                        "")),
                Word = "",
                XCordinateStart = 0,
                XCordinateEnd = 0,
                YCordinateEnd = 0,
                YCordinateStart = 0
            }).ToList();

            return rows;

        }
    }
}
