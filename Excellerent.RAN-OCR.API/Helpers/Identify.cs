﻿using Excellerent.RAN_OCR.API.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Excellerent.RAN_OCR.API.Helpers
{
    public class Identify
    {

        public  enum dataType
        {
            
            System_Int32 = 1,
            System_Int64 = 2,
            System_Double = 3,
            System_DateTime = 4,
            System_String = 5
        }
        public List<Row> RemoveSpace(List<Row> preSapce)

        {
            try
            {

                string findpreSpace = "";

                for (int i = 0; i <= preSapce.Count - 1; i++)
                {
                    if (preSapce[i].Word != null)
                    {
                        if (preSapce[i].Word.Trim() == ".")
                        {
                            if (Regex.IsMatch(preSapce[i-1].Word, @"^\$\d") || Regex.IsMatch(preSapce[i-1].Word, @"^\d"))
                                if (Regex.IsMatch(preSapce[i+1].Word, @"^\d{2}"))
                                {
                                    preSapce[i - 1].Word = preSapce[i - 1].Word + preSapce[i].Word + preSapce[i + 1].Word;
                                    preSapce[i - 1].Type = 3;
                                    preSapce[i].Word = "";
                                    preSapce[i].Type = 5;
                                    preSapce[i + 1].Word = "";
                                    preSapce[i + 1].Type = 5;
                                }
                        }
                        if (Regex.IsMatch(preSapce[i].Word, @"^\$\d$"))
                        {
                            if (Regex.IsMatch(preSapce[i + 1].Word, @"^\d\.$"))
                            {
                                preSapce[i + 1].Word = preSapce[i].Word + preSapce[i + 1].Word;
                                preSapce[i].Word = "";
                                preSapce[i].Type = 5;
                                if (Regex.IsMatch(preSapce[i + 2].Word, @"^\d"))
                                {
                                    preSapce[i + 2].Word = preSapce[i + 1].Word + preSapce[i + 2].Word;
                                    preSapce[i + 2].Type = 3;
                                    preSapce[i + 1].Word = "";
                                    preSapce[i + 1].Type = 5;

                                }


                            }
                        }
                        if (Regex.IsMatch(preSapce[i].Word.Trim(), @"^\.\d"))
                        {
                            findpreSpace = "async";
                            if (Regex.IsMatch(preSapce[i - 1].Word, @"^\d"))
                            {
                                preSapce[i - 1].Word =   preSapce[i - 1].Word + preSapce[i].Word;
                                preSapce[i].Word = "";
                                preSapce[i].Type = 5;
                            }
                         }

                        if (Regex.IsMatch(preSapce[i].Word.Trim(), @"^\d{1,3}\,\d{2}$") || Regex.IsMatch(preSapce[i].Word.Trim(), @"^\$\d{1,3}\,\d{2}$"))
                        {
                            Regex rgx2 = new Regex("\\,");
                            preSapce[i].Word = rgx2.Replace(preSapce[i].Word, ".");
                        }
                        //if (Regex.IsMatch(preSapce[i].Word.Trim(), @"^\d{1,3}\_\d{2}$") || Regex.IsMatch(preSapce[i].Word.Trim(), @"^\$\d{1,3}\_\d{2}$"))
                        //{
                        //    Regex rgx2 = new Regex("\\_");
                        //    preSapce[i].Word = rgx2.Replace(preSapce[i].Word, ".");
                        //}
                        
                    }
                }
                return preSapce;
            }
            catch
            {
                return preSapce;
            }
        }
        public dataType StringType(string str)
        {

            string[] formats = {"M/d/yyyy", "M/d/yyyy",
                   "MM/dd/yyyy","MM/dd/yy","M/dd/yy", "M/d/yyyy",
                   "M/d/yyyy", "M/d/yyyy",
                   "M/d/yyyy", "M/d/yyyy",
                   "MM/dd/yyyy", "M/dd/yyyy"};
            Int32 intValue;
            Int64 bigintValue;
            double doubleValue;
            DateTime dateValue;
            string Dstr = "";

            if (str!= null )
            { 
            Regex rgx = new Regex("\\$");
             Dstr = rgx.Replace(str, "");
            }

            if (str == "") return dataType.System_String;

            else if (Int32.TryParse(str, out intValue))
                return dataType.System_Int32;
            else if (Int64.TryParse(str, out bigintValue))
                return dataType.System_Int64;
            else if (double.TryParse(Dstr, out doubleValue))
                return dataType.System_Double;
            else if (DateTime.TryParseExact(str, formats,
                              new CultureInfo("en-US"),
                              DateTimeStyles.None, out dateValue))
                return dataType.System_DateTime;
            else return dataType.System_String;

        }


    }
}
