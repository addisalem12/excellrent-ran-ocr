﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActiveUp.Net.Mail;
using Excellerent.RAN_OCR.API.Data;
using Microsoft.EntityFrameworkCore;

namespace Excellerent.RAN_OCR.API.Repo
{
    public class DocumentRepository
    {
        public RANOcrContext _entities;
        public DocumentRepository(RANOcrContext context)// : base(context)
        {
            this._entities = context ?? throw new ArgumentNullException("context");
        }

        public async Task<Document> GetDocumentById(int id)
        {
            var result = (_entities.Set<Document>())
                    .Include(x => x.DocumentStatuses).FirstOrDefault(x => x.Id == id);
            return result;
        }

        public async Task<Document> GetDocumentByFileId(string fileId)
        {
            var result = (_entities.Set<Document>())
                    .Include(x => x.DocumentStatuses).FirstOrDefault(x => x.FileId == fileId);
            return result;
        }

        public virtual async Task<List<Document>> GetAllDocuments()
        {
            List<Document> query = (_entities.Set<Document>())
                .Include(x => x.DocumentStatuses).ToList();

            return query;
        }

        public virtual async Task<Document> AddDocument(Document model)
        {
            await _entities.Set<Document>().AddAsync(model);
            await _entities.SaveChangesAsync();

            return model;
        }

        public virtual async Task<DocumentStatus> AddDocumentStatus(DocumentStatus status)
        {
            await _entities.Set<DocumentStatus>().AddAsync(status);
            await _entities.SaveChangesAsync();

            return status;
        }

        public virtual async Task<Document> UpdateDocument(Document model)
        {
            var temp = await this.GetDocumentById(model.Id);
            _entities.Update(temp).CurrentValues.SetValues(model);
            //_entities.Entry(model).State = EntityState.Modified;
            await _entities.SaveChangesAsync();
            return model;
        }
    }
}