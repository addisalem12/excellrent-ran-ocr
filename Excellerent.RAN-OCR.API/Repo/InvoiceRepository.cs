﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActiveUp.Net.Mail;
using Excellerent.RAN_OCR.API.Data;
using Microsoft.EntityFrameworkCore;

namespace Excellerent.RAN_OCR.API.Repo
{
    public class InvoiceRepository
    {
        public RANOcrContext _entities;
        public InvoiceRepository(RANOcrContext context)// : base(context)
        {
            this._entities = context ?? throw new ArgumentNullException("context");
        }

        public async Task<Invoice> GetInvoiceById(int id)
        {
            var result = (_entities.Set<Invoice>())
                    .Include(x => x.LineItems).Include(x => x.Document).FirstOrDefault(x => x.Id == id);
            return result;
        }

        public virtual async Task<List<Invoice>> GetAllInvoices()
        {
            List<Invoice> query = (_entities.Set<Invoice>())
                .Include(x => x.LineItems).Include(x => x.Document).ToList();

            return query;
        }

        public virtual async Task<Invoice> AddInvoice(Invoice model)
        {
            await _entities.Set<Invoice>().AddAsync(model);
            await _entities.SaveChangesAsync();

            return model;
        }

        public virtual async Task<Invoice> UpdateInvoice(Invoice model)
        {
            var temp = await this.GetInvoiceById(model.Id);
            _entities.Update(temp).CurrentValues.SetValues(model);
            //_entities.Entry(model).State = EntityState.Modified;
            await _entities.SaveChangesAsync();
            return model;
        }
    }
}