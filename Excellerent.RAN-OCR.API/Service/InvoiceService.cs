﻿using Excellerent.RAN_OCR.API.Data;
using Excellerent.RAN_OCR.API.Enum;
using Excellerent.RAN_OCR.API.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Service
{
    public class InvoiceService
    {
        private readonly InvoiceRepository _repository;

        public InvoiceService(RANOcrContext context) {
            this._repository = new InvoiceRepository(context);
        }

        public async Task<List<Invoice>> SearchAll(int? documentId)
        {
            var result = (await this._repository.GetAllInvoices())
                    .Where(x => !documentId.HasValue || x.DocumentId == documentId.Value);
            return result.ToList();
        }

        public async Task<Invoice> Add(Invoice model)
        {
            var result = await this._repository.AddInvoice(model);
            return model;
        }

        public async Task<Invoice> Edit(Invoice model)
        {
            var result = await this._repository.UpdateInvoice(model);
            return result;
        }
    }
}
