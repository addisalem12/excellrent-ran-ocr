﻿using Excellerent.RAN_OCR.API.Data;
using Excellerent.RAN_OCR.API.Enum;
using Excellerent.RAN_OCR.API.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Excellerent.RAN_OCR.API.Service
{
    public class DocumentService
    {
        private readonly DocumentRepository _repository;

        public DocumentService(RANOcrContext context) {
            this._repository = new DocumentRepository(context);
        }

        public async Task<List<Document>> SearchAll(StatusEnum status)
        {
            var result = (await this._repository.GetAllDocuments())
                    .Where(x => status == StatusEnum.Unknown || x.DocumentStatuses.OrderByDescending(y => y.Date).First().Status == status);
            return result.ToList();
        }

        public async Task<Document> Add(Document model)
        {
            var result = await this._repository.AddDocument(model);
            return model;
        }

        public async Task<Document> Edit(Document model)
        {
            var result = await this._repository.UpdateDocument(model);
            return result;
        }

        public async Task<Document> UpdateStatus(int documentId, StatusEnum status)
        {
            DocumentStatus _status = new DocumentStatus()
            {
                DocumentId = documentId,
                Date = DateTime.UtcNow,
                Status = status,
            };
            var result = await this._repository.AddDocumentStatus(_status);
            return result.Document;
        }
    }
}
