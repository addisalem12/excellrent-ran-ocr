﻿using Excellerent.RAN_OCR.API.Data;
using Excellerent.RAN_OCR.API.Enum;
using Excellerent.RAN_OCR.API.Repo;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using System.Text.RegularExpressions;
using Google.Apis.Services;
using System.IO;
using Google.Apis.Auth.OAuth2;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Threading;
using Google.Apis.Util.Store;
using Excellerent.RAN_OCR.API.Model;
using Tesseract;
using System.Net.Http;
using Excellerent.RAN_OCR.API.Helpers;
using System.Xml;
using LineItem = Excellerent.RAN_OCR.API.Model.LineItem;


namespace Excellerent.RAN_OCR.API.Service
{
    public class OCRService
    {
        readonly DocumentRepository _documentRepository; 
        readonly InvoiceRepository _invoiceRepository;
        public const string folderName = "images/";
        public const string trainedDataFolderName = "tessdata";

        public OCRService(RANOcrContext context) {
            this._documentRepository = new DocumentRepository(context);
            this._invoiceRepository = new InvoiceRepository(context);
        }

        public OCRService(){ }

        public String DoOCR(OcrModel request)
        {
            string name = request.Image.FileName;
            var image = request.Image;

            if (image.Length > 0)
            {
                using (var fileStream = new FileStream(folderName + image.FileName, FileMode.Create))
                {
                    image.CopyTo(fileStream);
                }
            }

            string tessPath = Path.Combine(trainedDataFolderName, "");
            string result = "";

            using (var engine = new TesseractEngine(tessPath, request.DestinationLanguage, EngineMode.Default))
            {
                using (var img = Pix.LoadFromFile(folderName + name))
                {
                    var page = engine.Process(img);
                    result = page.GetText();
                    Console.WriteLine(result);
                }
            }
            return String.IsNullOrWhiteSpace(result) ? "Ocr is finished. Return empty" : result;

        }

        public async Task<string> ExtractInvoiceAsync(string dir)
        {
            var client = new HttpClient();
            if (!string.IsNullOrEmpty(dir))
            {
                client.BaseAddress = new Uri("https://api.ocr.space/");
                client.DefaultRequestHeaders.Accept.Clear();

                var multiContent = new MultipartFormDataContent();
                var file = new StreamReader(dir);
                if (file != null)
                {
                    var fileStreamContent = new StreamContent(file.BaseStream);
                    multiContent.Add(fileStreamContent, "fileToUpload", dir);

                    multiContent.Add(new StringContent("true"), "isoverlayrequired");
                    multiContent.Add(new StringContent("true"), "istable");

                }

                multiContent.Headers.Add("apiKey", "c9cf1511a288957");

                var response = await client.PostAsync("parse/image", multiContent);
                if (response.IsSuccessStatusCode)
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    var ocrResult = OcrResult.FromJson(apiResponse);
                    //var lines = ocrResult.ParsedResults[0].TextOverlay.Lines;
                    string fullData = ocrResult.ParsedResults[0].ParsedText;
                    string[] lines = fullData.Split('\n');
                    var rowlist = new List<Row>();
                    Identify strType = new Identify();
                    for (int lineIndex = 0; lineIndex < lines.Length; lineIndex++)
                    {
                        string line = lines[lineIndex].Replace('\r', ' ');
                        string[] lineText = line.Split('\t');
                        for(int lineTextIndex=0; lineTextIndex < lineText.Length; lineTextIndex ++)
                        {
                            Row newRow = new Row
                            {
                                Line = lineIndex,
                                Colomun = lineTextIndex,
                                Type = Convert.ToInt16(strType.StringType(lineText[lineTextIndex])),
                                Word = lineText[lineTextIndex],
                                XCordinateStart = 0,
                                XCordinateEnd = 0,
                                YCordinateStart = 0,
                                YCordinateEnd = 0


                            };
                            rowlist.Add(newRow);
                        }

                    }
                    string fileName = Path.GetFileName(dir);
                     await PopulateInvoice(rowlist, fileName);
                }
            }
                return string.Empty;
        }

        public async Task PopulateInvoice(List<Row> pageItem, string fileName)
        {

            bool isNull = pageItem == null;
            try
            {
                int whereTotal = 0;
                Regex rgx = new Regex("\\$");
                var total = new List<string> { "Total", "Total dues" };
                Model.Invoice currentInvoice = new Model.Invoice();
                for (int i = pageItem.Count - 1; i >= 0; i--)
                {
                    if (pageItem[i].Word != null)

                    {
                        if ((pageItem[i].Word.ToUpper() == "TOTAL".ToUpper() ||
                            pageItem[i].Word.ToUpper() == "T0TAL:".ToUpper() ||
                            pageItem[i].Word.ToUpper() == "T0ta1".ToUpper() ||
                            pageItem[i].Word.ToUpper() == "TOTA1".ToUpper() ||
                            pageItem[i].Word.ToUpper() == "payment".ToUpper()
                           ) && currentInvoice.TotalAmount == null)
                        {
                            whereTotal = i;
                            for (int j = whereTotal; j <= pageItem.Count - 1; j++)
                            {
                                if (pageItem[whereTotal].Line != pageItem[j].Line) break;

                                if (pageItem[j].Type == 3)
                                {


                                    currentInvoice.TotalAmount = rgx.Replace(pageItem[j].Word, "");
                                    int whereLineItem = 0;
                                    double itemAmount = 0;

                                    string iteamName = "";
                                    double itemQty = 0;

                                    for (int k = whereTotal; k >= 0; k--)
                                    {

                                        if (whereLineItem != 0 && whereLineItem != pageItem[k].Line)
                                        {
                                            whereLineItem = 0;
                                            Model.LineItem lineIteam = new Model.LineItem();
                                            lineIteam.ItemAmount = itemAmount;
                                            lineIteam.ItemQty = itemQty == 0 ? 1 : itemQty;
                                            lineIteam.ItemName = iteamName;
                                            if (Convert.ToDouble(currentInvoice.TotalAmount) >= lineIteam.ItemAmount)
                                                currentInvoice.Lineitems.Add(lineIteam);

                                            iteamName = "";
                                        }

                                        if (pageItem[whereTotal].Line != pageItem[k].Line)
                                        {


                                            if (pageItem[k].Type == 3 && whereLineItem == 0)
                                            {
                                                whereLineItem = pageItem[k].Line;


                                                string str = rgx.Replace(pageItem[k].Word, "");
                                                itemAmount = Convert.ToDouble(str);


                                            }

                                            if (whereLineItem == pageItem[k].Line)
                                            {

                                                if (pageItem[k].Type == 1 || pageItem[k].Type == 2)
                                                    itemQty = Convert.ToDouble(pageItem[k].Word);
                                                else if (pageItem[k].Type == 5 && pageItem[k].Word != "$")
                                                    iteamName = pageItem[k].Word + " " + iteamName;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (pageItem[i].Type == 4)
                        {
                            //IFormatProvider monthFirst = IFormatProvider("MM/dd/yyyy");
                            currentInvoice.InvoiceDate = pageItem[i].Word;
                        }

                    }
                }
                if (currentInvoice.InvoiceDate == null)
                {
                    string month = "";
                    var months = new List<string>
                    {
                        "JAN",
                        "FEB",
                        "MAR",
                        "APR",
                        "MAY",
                        "JUN",
                        "JUL",
                        "AUG",
                        "SEP",
                        "OCT",
                        "NOV",
                        "DEC"
                    };
                    for (int i = 0; i <= pageItem.Count - 1; i++)
                    {
                        // pageItem[i].Word.StartsWith<monts>.any;
                        if (pageItem[i].Word != null)
                        {
                            string muchWord = pageItem[i].Word.ToUpper();
                            foreach (string mon in months)
                            {
                                if (muchWord.StartsWith(mon))
                                {
                                    if (Regex.IsMatch(muchWord, ".*\\d{2}") || muchWord == mon)
                                    {
                                        if (Regex.IsMatch(pageItem[i - 1].Word, @"^\d{2}$"))

                                            month = pageItem[i - 1].Word + " " + pageItem[i].Word + " " +
                                                    pageItem[i + 1].Word;
                                        else if (Regex.Matches(pageItem[i + 1].Word, @"[a-zA-Z]").Count <= 2 &&
                                                 Regex.Matches(pageItem[i + 1].Word, @"[0-9]").Count >= 2)
                                            month = pageItem[i].Word + " " + pageItem[i + 1].Word;
                                        else
                                            month = pageItem[i].Word;
                                    }

                                    break;
                                }
                            }
                            // if (muchWord.Where(s => months.Any(e => s.StartsWith(e))))//(months.Any(muchWord.Contains))


                        }

                    }
                    if (month != "") currentInvoice.InvoiceDate = month;
                }
                int currentLine = 0;
                string vName = "";
                for (int i = 0; i <= pageItem.Count - 1; i++)
                {
                    if (vName != "")
                    {
                        if (currentLine != pageItem[i].Line && currentLine != 0)
                        {
                            if (vName.Trim().Length > 3 && Regex.Matches(vName, @"[a-zA-Z]").Count > 3)
                            // check if it is valid Vendorname 
                            {

                                currentInvoice.VendorName = vName;
                                break;
                            }
                            else vName = "";
                        }
                    }
                    if (pageItem[i].Type == 5 && pageItem[i].Word != null)
                    {
                        if (pageItem[i].Word.Trim() != "")
                            vName = vName + " " + pageItem[i].Word;
                        if (currentLine == 0)
                            currentLine = pageItem[i].Line;
                    }
                }
                currentInvoice.InvoiceID = fileName;
                if (currentInvoice.TotalAmount == null)
                {
                    //advanced Total finder 
                    for (int i = pageItem.Count - 1; i >= 0; i--)
                    {

                        if (pageItem[i].Type == 3 && currentInvoice.TotalAmount == null)
                        {

                            currentInvoice.TotalAmount = rgx.Replace(pageItem[i].Word, "");
                            int whereLineItem = 0;
                            double itemAmount = 0;

                            string iteamName = "";
                            double itemQty = 0;
                            whereTotal = i;
                            for (int k = whereTotal; k >= 0; k--)
                            {

                                if (whereLineItem != 0 && whereLineItem != pageItem[k].Line)
                                {
                                    whereLineItem = 0;
                                    Model.LineItem lineIteam = new LineItem();
                                    lineIteam.ItemAmount = itemAmount;
                                    lineIteam.ItemQty = itemQty == 0 ? 1 : itemQty;
                                    lineIteam.ItemName = iteamName;
                                    if (Convert.ToDouble(currentInvoice.TotalAmount) >= lineIteam.ItemAmount)
                                        currentInvoice.Lineitems.Add(lineIteam);

                                    iteamName = "";
                                }

                                if (pageItem[whereTotal].Line != pageItem[k].Line)
                                {
                                    if (pageItem[k].Type == 3 && whereLineItem == 0)
                                    {
                                        whereLineItem = pageItem[k].Line;
                                        string str = rgx.Replace(pageItem[k].Word, "");
                                        itemAmount = Convert.ToDouble(str);

                                    }

                                    if (whereLineItem == pageItem[k].Line)
                                    {

                                        if (pageItem[k].Type == 1 || pageItem[k].Type == 2)
                                            itemQty = Convert.ToDouble(pageItem[k].Word);
                                        else if (pageItem[k].Type == 5 && pageItem[k].Word != "$")
                                            iteamName = pageItem[k].Word + " " + iteamName;

                                    }
                                }
                            }
                        }
                    }

                }
                Model.Invoice arranged = new Model.Invoice();
                //for (int i = currentInvoice.Lineitems.Count; i <0 ; i++)
                //{
                //    Lineitem lineIteam = currentInvoice.Lineitems[i];
                //    arranged.Lineitems.Add();
                //}
                foreach (var source in currentInvoice.Lineitems.Reverse().AsEnumerable())
                {

                    arranged.Lineitems.Add(source);

                }
                arranged.InvoiceDate = currentInvoice.InvoiceDate;
                arranged.InvoiceID = currentInvoice.InvoiceID;
                arranged.TotalAmount = currentInvoice.TotalAmount;
                arranged.VendorName = currentInvoice.VendorName;
                //eventLog.EvenEndAt = DateTime.Now;
                //eventLog.IsSuccessful = true;
                //eventLog.Description = pageItem.Count+"------"+ "Successfully Values Extracted";
                // PostResult.PostEventApi(eventLog);
                XmlDocument doc = new XmlDocument();
                //create the serialiser to create the xml
                //XmlSerializer serialiser = new XmlSerializer(typeof(Invoice2));
                if (!Directory.Exists(@"C:\TEST\FinalOutput\"))
                    Directory.CreateDirectory(@"C:\TEST\FinalOutput\");
                // Create the TextWriter for the serialiser to use
                TextWriter filestream = new StreamWriter(@"C:\TEST\FinalOutput\" + Path.GetFileNameWithoutExtension(fileName) + ".xml");


                // Close the file
                filestream.Close();

                var document = await this._documentRepository.GetDocumentByFileId(Path.GetFileNameWithoutExtension(fileName));
                if (document != null) 
                {
                    try
                    {
                        var invoice = new Data.Invoice()
                        {
                            InvoiceDate = DateTime.UtcNow,
                            TotalAmount = decimal.Parse(arranged.TotalAmount),
                            InvoiceID = arranged.InvoiceID,
                            DocumentId = document.Id,
                            LineItems = new List<Data.LineItem>()
                        };

                        foreach (var l in arranged.Lineitems)
                        {
                            invoice.LineItems.Add(new Data.LineItem()
                            {
                                ItemAmount = (decimal?)l.ItemAmount,
                                ItemName = l.ItemName,
                                ItemQty = (decimal?)l.ItemQty
                            });
                        }

                        await this._invoiceRepository.AddInvoice(invoice);
                    }
                    catch (Exception e) { 
                        
                    }                   
                }
            }
            catch (Exception ex)
            {
                //eventLog.EvenEndAt = DateTime.Now;
                //eventLog.IsSuccessful = false;
                //eventLog.Description =   ex.ToString();
                //PostResult.PostEventApi(eventLog);
                //return null;
            }

        }
    }
}
